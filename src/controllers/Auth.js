const User = require("../models/user")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const ROLES = require("../utils/Roles")

module.exports.login = (req, res) => {

    User.findOne({ email: req.body.email })
        .exec()
        .then((user) => {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                let token = jwt.sign({ id: user._id }, process.env.JWT_SIG_KEY)
                console.log(token)
                res.status(200).send({ token: token, message: "login successfully", name: user.name })
            } else
                res.status(401).send("incorrect password")
        })
}

module.exports.checkToken = (req, res, next) => {
    let token = req.headers.token
    jwt.verify(token, process.env.JWT_SIG_KEY, (error, decode) => {
        if (error) {
            res.status(401).send("Token invalido")
        } else {
            next()
        }

    })
}

module.exports.checkUserRole = (req, res, next) => {
    let token = req.headers.token
    let payload = jwt.decode(token)
    let user_id = payload.id

    let promise = User.findById(user_id).exec()
    promise.then((user) => {
        if(user.role == ROLES.ADMIN || user.role == ROLES.USER)
            next()
        else
            return res.status(401).json({ message: "unauthorized", error: error })
    }).catch((error) => {
        res.status(401).json({ message: "unauthorized", error: error })
    })

}

module.exports.checkAdminRole = (req, res, next) => {
    let token = req.headers.token
    let payload = jwt.decode(token)
    let user_id = payload.id

    let promise = User.findById(user_id).exec()
    promise.then((user) => {
        if(user.role == ROLES.ADMIN)
            next()
        else    
            return res.status(401).json({ message: "unauthorized", error: error })
    }).catch((error) => {
        res.status(401).json({ message: "unauthorized", error: error })
    })

}