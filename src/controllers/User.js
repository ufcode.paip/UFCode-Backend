const User = require('../models/user')
const view = require('../views/user')
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken")
const ROLES = require("../utils/Roles.js")

module.exports.createtUser = (req, res) => {
    let user = {
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        role: ROLES.USER
    }
    let promise = User.create(user)

    console.log(promise)
    promise.then((user) => {
        res.status(201).json(view.render(user))
    }).catch((error) => {
        res.status(400).json({ message: "error message" })
    })

}

module.exports.listUser = (req, res) => {
    let promise = User.find().exec()
    promise.then((user) => {
        res.status(200).json(view.renderMany(user))
    }).catch((error) => {
        res.status(400).json({ message: "error message", error: error })
    })
}
module.exports.updateUser = (req, res) => {
    console.log("ta rodando isso aqui?")
    let id = req.params.id
    let body = req.body
    let promise = User.findByIdAndUpdate(id, body, { new: true }).exec()
    promise.then((user) => {
        res.status(200).json(view.render(user))
    }).catch((error) => {
        res.status(400).json({ message: "user not found", error: error })
    }
    )
}

module.exports.findUser = (req, res) => {
    let id = req.params.id
    let promise = User.findById(id).exec()
    promise.then((user) => {
        res.status(200).json(view.render(user))
    }).catch((error) => {
        res.status(404).json({ message: "user not found", error: error })
    }
    )
}