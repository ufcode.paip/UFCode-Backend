let controller = require("../controllers/Module.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)
        .get("/api/module", auth.checkUserRole, controller.listModule)
        .get("/api/module/:id", auth.checkUserRole, controller.findModule)
        .post("/api/module", auth.checkAdminRole, controller.createModule)
        .put("/api/module/:id", auth.checkAdminRole, controller.updateModule)
        .delete("/api/module/:id", auth.checkAdminRole, controller.deleteModule)

}