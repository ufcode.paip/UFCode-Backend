let controller = require("../controllers/Problem.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)
        .get("/api/problem", auth.checkUserRole, controller.listProblem)
        .get("/api/problem/:id", auth.checkUserRole, controller.findProblem)
        .post("/api/problem", auth.checkAdminRole, controller.createProblem)
        .delete("/api/problem/:id", auth.checkAdminRole, controller.deleteProblem)
        .put("/api/problem/:id", auth.checkAdminRole, controller.updateProblem)
        .get("/api/problem/:courseId/:moduleId", auth.checkAdminRole, controller.findProblemByCourseAndModule)
}