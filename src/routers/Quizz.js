let controller = require("../controllers/Quizz.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)
        .get("/api/quizz", auth.checkUserRole, controller.listQuizz)
        .get("/api/quizz/:id", auth.checkUserRole, controller.findQuizz)
        .post("/api/quizz", auth.checkAdminRole, controller.createQuizz)
        .delete("/api/quizz/:id", auth.checkAdminRole, controller.deleteQuizz)

}