let controller = require("../controllers/User")
let auth = require('../controllers/auth')
module.exports = (app) => {

    app.post("/api/user/signin", auth.login)
        .post("/api/user", controller.createtUser)
        .use("/api/user/", auth.checkToken)
        .get("/api/user", controller.listUser)
        .get("/api/user/:id", controller.findUser)
        .put("/api/user/:id", controller.updateUser)

}