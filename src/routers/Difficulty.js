let controller = require("../controllers/Difficulty.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)
        .get("/api/difficulty", auth.checkUserRole, controller.listDifficulty)
        .get("/api/difficulty/:id", auth.checkUserRole, controller.findDifficulty)
        .post("/api/difficulty", auth.checkAdminRole, controller.createDifficulty)
        .put("/api/difficulty/:id", auth.checkAdminRole, controller.updateDifficulty)
        .delete("/api/difficulty/:id", auth.checkAdminRole, controller.deleteDifficulty)
}