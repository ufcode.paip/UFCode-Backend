let controller = require("../controllers/Course.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)
        .get("/api/course", auth.checkUserRole, controller.listCourse)
        .get("/api/course/:id", auth.checkUserRole, controller.findCourse)
        .post("/api/course", auth.checkAdminRole, controller.createCourse)
        .put("/api/course/:id", auth.checkAdminRole, controller.updateCourse)
        .delete("/api/course/:id", auth.checkAdminRole, controller.deleteCourse)

}