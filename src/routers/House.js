let controller = require("../controllers/House.js")
let auth = require('../controllers/auth')

module.exports = (app) => {

    app.use("/api/course/", auth.checkToken)

        .get("/api/house",auth.checkUserRole, controller.listHouse)
        .get("/api/house/:id", auth.checkUserRole, controller.findHouse)
        .post("/api/house", auth.checkAdminRole, controller.createHouse)
        .put("/api/house/:id", auth.checkAdminRole, controller.updateHouse)
        .delete("/api/house/:id", auth.checkAdminRole, controller.deleteHouse)
}